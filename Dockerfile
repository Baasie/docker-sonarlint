FROM java:8-jdk

MAINTAINER Kenny Baas <kenny@baasie.com>

ENV PATH=$PATH:/opt/sonarlint/sonarlint-cli-2.1.0.566/bin \
    SONARLINT_VERSION="2.1.0.566"

RUN apt-get update && apt-get install -y unzip && \
    wget --quiet "https://sonarsource.bintray.com/Distribution/sonarlint-cli/sonarlint-cli-${SONARLINT_VERSION}.zip" -O sonarlint.zip && \
   	mkdir /opt/sonarlint &&\
    unzip -d opt/sonarlint sonarlint.zip && \
    rm sonarlint.zip